# kuncie music player

This app has been built with Flutter 2.2.2

# Supported Devices
This app support iOS and Android

This app running well on:
    - iPhone 13 Pro Max (iOS 15.2) Emulator
    - Android Poco Phone F1 (Android 10)

# Supported Features
This app uses GetX for State Management Manager
You can search and play all songs or artists in this app connected to iTunes API

# Requirements
Min OS Android 5.0 or iOS 12.0


