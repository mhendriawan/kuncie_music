import 'dart:convert';

import 'package:get/get.dart';

class HomeConnect extends GetConnect {
  fetchMusic(search) async {
    var url = 'https://itunes.apple.com/search';
    var params = '$url?term=$search&entity=song&country=id';

    var res = await get(params);
    var data = jsonDecode(res.body);

    return data;
  }
}
