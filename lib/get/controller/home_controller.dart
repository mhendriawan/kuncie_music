import 'package:audioplayer/audioplayer.dart';
import 'package:get/get.dart';
import 'package:kuncie/get/connect/home_connect.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.find();

  AudioPlayer audioPlayer = AudioPlayer();
  String urlGif = 'https://radiotop.sukoharjokab.go.id/imgs/music2.gif';
  String urlMusic = '';
  String artists = '';
  String songName = '';
  int lengthListMusic = 0;
  int indexSelected;
  bool isPlay = false;
  bool isShow = false;
  List listMusic = [];
  List isPlayWave = [];

  void playMusic(previewUrl, artistName, trackName, index) async {
    await audioPlayer.stop();
    if (indexSelected != null) {
      isPlayWave[indexSelected] = false;
    }
    isShow = true;
    urlMusic = previewUrl;
    artists = artistName;
    songName = trackName;
    indexSelected = index;
    isPlayWave[index] = true;

    play();
    update();
  }

  void searchMusic(search) async {
    await audioPlayer.stop();
    isPlayWave.clear();
    isShow = false;

    HomeConnect().fetchMusic(search).then((value){
      listMusic.insert(0, value['results']);
      lengthListMusic = listMusic[0].length;

      for(var i=0; i<lengthListMusic; i++){
        isPlayWave.insertAll(0, [false]);
      }

      update();
    }).catchError((error){
      Get.snackbar('errorFetch', error.toString());
    });
  }

  void play() async {
    await audioPlayer.play(urlMusic);
    isPlay = true;
    isPlayWave[indexSelected] = true;
    update();
  }

  void paused() async {
    await audioPlayer.pause();
    isPlay = false;
    isPlayWave[indexSelected] = false;
    update();
  }
}
