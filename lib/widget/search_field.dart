import 'package:flutter/material.dart';
import 'package:kuncie/get/controller/home_controller.dart';

class SearchField extends StatefulWidget {

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  TextEditingController _txtSearch = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Container(
        width: MediaQuery.of(context).size.width - 100,
        child: TextFormField(
          controller: _txtSearch,
          onFieldSubmitted: (value) => HomeController.to.searchMusic(value),
          decoration: InputDecoration(
            hintText: 'Search Music',
            suffixIcon: IconButton(
              onPressed: () => HomeController.to.searchMusic(_txtSearch.text),
              icon: Icon(Icons.search),
            ),
            contentPadding: EdgeInsets.only(left: 10),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      ),
    );
  }
}
