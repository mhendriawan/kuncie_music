import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuncie/get/controller/home_controller.dart';
import 'package:kuncie/widget/search_field.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    Get.put(HomeController());

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(child: Container()),
          Expanded(flex: 8, child: _body(width, context)),
        ],
      ),
    );
  }

  Widget _body(width, context) {
    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.topCenter,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              topLeft: Radius.circular(30),
            ),
          ),
          child: _listMusic(context, width),
        ),
        Positioned(top: -20, child: SearchField())
      ],
    );
  }

  Widget _listMusic(context, width) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (controller) => MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: Column(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
                child: (controller.lengthListMusic != 0)
                    ? ListView.separated(
                        separatorBuilder: (c, i) => Divider(thickness: 2),
                        itemCount: controller.lengthListMusic,
                        itemBuilder: ((c, i) {
                          var data = controller.listMusic[0][i];
                          return GestureDetector(
                            onTap: () => controller.playMusic(
                              data['previewUrl'],
                              data['artistName'],
                              data['trackName'],
                              i,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Image.network(data['artworkUrl100']),
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  flex: 5,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(data['trackName']),
                                      Text(data['artistName']),
                                      Text(data['collectionName']),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 10),
                                (controller.isPlayWave.length != 0)
                                    ? Expanded(
                                        flex: 2,
                                        child: (controller.isPlayWave[i] !=
                                                false)
                                            ? Image.network(controller.urlGif)
                                            : Text(''),
                                      )
                                    : Text('')
                              ],
                            ),
                          );
                        }),
                      )
                    : Center(child: Text('No Music')),
              ),
            ),
            (controller.isShow) ? _playMusic(width, controller) : Text('')
          ],
        ),
      ),
    );
  }

  Widget _playMusic(width, controller) {
    return Container(
      color: Colors.grey[100],
      width: width,
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            Text('${controller.artists} - ${controller.songName}'),
            SizedBox(height: 10),
            GestureDetector(
              onTap: controller.isPlay
                  ? () => controller.paused()
                  : () => controller.play(),
              child: controller.isPlay
                  ? Icon(Icons.pause, size: 35)
                  : Icon(Icons.play_circle_fill, size: 35),
            )
          ],
        ),
      ),
    );
  }
}
